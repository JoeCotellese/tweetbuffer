import pytest
import shutil
from persistqueue import Queue
from buffer.tweet_queue import TweetQueue

@pytest.fixture()
def queue():
    queue = Queue('tests/tweets_queue')
    yield queue
    shutil.rmtree('tests/tweets_queue')

def test_queue(queue):
    tweet = "this is a test"
    q = TweetQueue(queue)
    q.put(tweet)
    out = q.get()
    assert tweet == out

def test_put(queue):
    tweet = "this is a test"
    q = TweetQueue(queue)
    q.put(tweet)
    q.get()
    q.put(tweet)
    assert q.queue.qsize() == 1