# TweetBuffer

A simple app that sends tweets on a schedule

I got tired of paying for products like Buffer and SocialPilot. I only needed them for a simple use case - periodically sending a tweet. So, I decided to scratch an itch and build my own.

## Installation 

```sh
pip install pipenv
pipenv --three install
```

## Usage
This tool requires a Twitter [developer account](https://developer.twitter.com) Setting one up is left as an exercise to the reader.

Once you have a developer account, stuff the keys in an .env file. A sample is included.

### Scheduling
There is a default schedule in the config.yml. It uses a cron style syntax. Customize it as you see fit.

### Getting Help
```sh
pipenv run python -m buffer --help
```

### Add a tweet to the queue
```sh
pipenv run python -m buffer --add "This is a tweet"
```

### Run the scheduler

```sh
pipenv run python -m buffer --scheduler start
```

## TODO

* The scheduler blocks, it would be better to make this a background process.
* Maybe change this to use Selenium so you don't have to deal with the Twitter API?
