import logging
import argparse
import yaml
from apscheduler.schedulers.background import BlockingScheduler
from persistqueue import Queue
from .tweet_queue import TweetQueue
from .tweeter import Tweeter
logging.basicConfig(filename='logs/tweeter.log',level=logging.INFO)

def next_tweet(queue, tweeter):
    if queue.queue.qsize() > 0:
        tweet = queue.get()
        tweeter.publish(tweet)
        queue.queue.task_done()
    else:
        logging.info("Queue empty, not publishing a tweet")
    return

if __name__ == "__main__":
    queue = Queue('data/queue')
    tq = TweetQueue(queue)
    tweeter=Tweeter()
    with open ('config.yml') as c:
        config = yaml.load(c)
    parser = argparse.ArgumentParser(description = "A Buffer clone that tweets on a schedule")
    parser.add_argument("--scheduler", help="manages the scheduler", choices=["start","stop"])
    parser.add_argument("--tweet", help="send next tweet immediately")
    parser.add_argument("--add", help="Add a tweet to the queue")
    parser.add_argument("--size", help="returns the size of the queue", action="store_true")
    args = parser.parse_args()
    if args.size:
        print (tq.queue.qsize())
        exit()
    if (args.add is not None) and len(args.add) >0:
        print ("Add to queue: {}".format(args.add))
        tq.put(args.add)
        logging.info("Add to queue: {}".format(args.add))
        exit()
    
    if args.scheduler == 'start':
        s=config['schedule']
        dow = s['day_of_week']
        j = s['jitter']
        h = s['hour']
        m=s['minute']
        scheduler = BlockingScheduler()
        scheduler.add_job(next_tweet, 'cron', day_of_week=dow, hour=h, minute=m, jitter=j, args=[tq,tweeter])
        scheduler.start()

    if args.tweet:
        next_tweet(tq, tweeter)
