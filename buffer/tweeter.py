import logging
import os
import twitter

class Tweeter(object):
    def __init__(self):
        api_key = os.environ['TWITTER_APIKEY']
        api_secret = os.environ['TWITTER_APISECRET']
        at_key = os.environ['TWITTER_ATKEY']
        at_secret = os.environ['TWITTER_ATSECRET']
        self.api = twitter.Api(consumer_key=api_key,
                    consumer_secret=api_secret,
                    access_token_key=at_key,
                    access_token_secret=at_secret)
    def publish(self, tweet):
        logging.info(tweet)
        status = self.api.PostUpdate(tweet)


