from persistqueue import Queue

class TweetQueue:
    TWEET_LENGTH=200
    queue = None
    def __init__(self, queue):
        self.queue= queue
    def put(self, tweet):
        if len(tweet) > self.TWEET_LENGTH:
            raise ValueError("the tweet is too long it should be less than 200 characters")
        self.queue.put(tweet)
    def get(self):
        return self.queue.get()